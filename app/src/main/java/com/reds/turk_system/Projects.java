package com.reds.turk_system;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class Projects extends Fragment { // the projects screen presented to the client with a list of all their projects.
    ListView list;
    ArrayList<String> data;
    Button new_proj;

    public Projects() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_projects, container, false);
        list = (ListView) root.findViewById(R.id.proj_list);
        new_proj = (Button) root.findViewById(R.id.newproj);
        data = getArguments().getStringArrayList("data");
        new DataRetrieval(list, 3).execute(data.get(1));
        new_proj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { // a click listener for creating a new project
                Intent jump = new Intent(getContext(), Create_Proj.class);
                jump.putExtra("un", data.get(1));
                jump.putExtra("bank", data.get(8));
                startActivity(jump);
            }
        });
        list.setOnItemClickListener(new Project_Viewer(data));
        return root;
    }

}
