package com.reds.turk_system;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Banker extends AppCompatActivity { // This is the activity the user sees when editing their bank information
    private EditText amount;
    private Button sub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banker);
        amount = (EditText) findViewById(R.id.amount);
        sub = (Button) findViewById(R.id.subamount);
        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (amount.getText().toString().compareTo("") == 0) {
                    amount.setError("Enter An amount ");
                } else {
                    new DataSender(sub.getContext(), 14).execute(getIntent().getExtras().get("usernum"), amount.getText().toString());
                    finish();
                }
            }
        });

    }
}
