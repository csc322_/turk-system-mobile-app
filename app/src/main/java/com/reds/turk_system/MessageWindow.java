package com.reds.turk_system;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MessageWindow extends AppCompatActivity { // simple message windows for sending messages to the user.
    private TextView window;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_window);
        window = (TextView) findViewById(R.id.messwind);
        window.setText(getIntent().getExtras().get("message").toString());
    }
}
