package com.reds.turk_system;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class PlaceBid extends AppCompatActivity { // Screen presented to Devs when they place a bid on a project.
    private TextView min, max;
    private Button submit;
    private SeekBar value;
    private String[] data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_bid);
        //setup
        min = (TextView) findViewById(R.id.Min);
        max = (TextView) findViewById(R.id.Max);
        submit = (Button) findViewById(R.id.placebidbut);
        value = (SeekBar) findViewById(R.id.seekBar3);
        min.setText("0");
        data = getIntent().getExtras().getStringArray("projdata");
        max.setText(data[6]);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { // button clicker to submit value of bid.
                float bidval = (Float.parseFloat(data[6])) * ((float) value.getProgress() / (float) value.getMax());

                new DataSender(submit.getContext(), 5).execute(getIntent().getExtras().get("usernum").toString(), data[1], bidval + "");
            }
        });

    }
}
