package com.reds.turk_system;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class Ratings extends AppCompatActivity { // a screen presented to a user with a list of ratings towards them.
    private ListView ratings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ratings);
        ratings = (ListView) findViewById(R.id.ratinglist);
        new DataRetrieval(ratings, 16).execute(getIntent().getExtras().get("usernum"));
    }
}
