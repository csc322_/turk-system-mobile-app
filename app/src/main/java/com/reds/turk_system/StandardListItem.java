package com.reds.turk_system;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.zip.Inflater;

/**
 * Created by Ronny on 11/18/2017.
 */

public class StandardListItem extends BaseAdapter {// the item type used in the search list.
    private String[] data;
    private Context cont;
    private LayoutInflater inflater;
    private TextView id, name, type;

    public StandardListItem(Context cont, String[] data) {
        this.data = data;
        this.cont = cont;
        inflater = (LayoutInflater) cont.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int i) {
        return data[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = view;
        if (vi == null)
            vi = inflater.inflate(R.layout.standard_item, null);
        id = (TextView) vi.findViewById(R.id.proj_id);
        name = (TextView) vi.findViewById(R.id.name_user);
        type = (TextView) vi.findViewById(R.id.type_id);
        String temp[] = data[i].split(",");
        id.setText("ID:" + temp[1]);
        name.setText(temp[3]);
        type.setText(temp[11]);

        return vi;

    }
}
