package com.reds.turk_system;

import android.content.Intent;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

public class Mainpage extends AppCompatActivity { // The over all layout incharge of presenting screens to the users .
    private BottomNavigationView nav;
    private Fragment main_frag;
    private Bundle info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainpage);
        info = new Bundle();
        info.putStringArrayList("data", getIntent().getExtras().getStringArrayList("data1"));
        main_frag = new Homepage();
        main_frag.setArguments(info);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_view, main_frag)
                .setTransition(FragmentTransaction.TRANSIT_EXIT_MASK)
                .commit();

        nav = (BottomNavigationView) findViewById(R.id.nav);
        switch (getIntent().getExtras().getStringArrayList("data1").get(3)) { // determines the type of user and the type of menu to present.
            case "dev":
                nav.inflateMenu(R.menu.dev_menu);
                break;
            case "client":
                nav.inflateMenu(R.menu.client_menu);
                break;
            case "su":
                nav.inflateMenu(R.menu.su_menu);
                break;
            case "black":
                Intent jump = new Intent(nav.getContext(), FakeMainPage.class);
                jump.putExtra("message", "You Were placed in the Black List Try Again In A YEAR!");
                startActivity(jump);
                break;
            case "rejected":
                Intent jump2 = new Intent(nav.getContext(), FakeMainPage.class);
                jump2.putExtra("message", "You Were Rejected Please Try Agian");
                startActivity(jump2);
                break;
            case "temp":
                nav.inflateMenu(R.menu.temp_menu);
                break;
            case "visitor":
                nav.inflateMenu(R.menu.visitor_menu);
                break;


        }
        nav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) { // determines navigation when menu item is clicked.

                switch (item.getItemId()) {
                    case R.id.profile:
                        main_frag = new Profile();
                        break;
                    case R.id.search:
                        main_frag = new Search();
                        break;
                    case R.id.Bid:
                        main_frag = new Bid();
                        break;
                    case R.id.projects:
                        main_frag = new Projects();
                        break;
                    case R.id.Disputes:
                        main_frag = new Warnings();
                        break;
                    case R.id.Authorize:
                        main_frag = new Authorize();
                        break;
                    case R.id.Quit:
                        main_frag = new Quit();
                        break;
                    case R.id.Payment:
                        main_frag = new Payment();
                        break;
                    case R.id.BlackList:
                        main_frag = new Blacklist();
                        break;
                    default:
                        main_frag = new Homepage();
                        break;

                }
                main_frag.setArguments(info); // changes the screen that is present to the user after a menu item is clicked
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container_view, main_frag)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                return true;
            }
        });

    }
}
