package com.reds.turk_system;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Edit_Bio extends AppCompatActivity { // This screen is shown to users when they are editing their bio's
    private EditText bio;
    private Button update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_bio);
        bio = (EditText) findViewById(R.id.Biotext);
        bio.setText(getIntent().getExtras().getStringArray("bio")[0]);
        update = (Button) findViewById(R.id.update);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DataSender(update.getContext(), 0).execute(bio.getText().toString(), getIntent().getExtras().getStringArray("bio")[1]);
            }
        });


    }
}
