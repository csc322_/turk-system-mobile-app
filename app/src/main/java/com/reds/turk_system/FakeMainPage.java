package com.reds.turk_system;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

public class FakeMainPage extends AppCompatActivity { //This is the home screen presented to visitors.
    private BottomNavigationView nav;
    private Fragment main_frag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fake_main_page);
        main_frag = new Homepage();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fakecontainer_view, main_frag)
                .setTransition(FragmentTransaction.TRANSIT_EXIT_MASK)
                .commit();
        nav = (BottomNavigationView) findViewById(R.id.fakenav);
        nav.inflateMenu(R.menu.visitor_menu);
        if (getIntent().hasExtra("message")) {
            Intent jump = new Intent(nav.getContext(), MessageWindow.class);
            jump.putExtra("message", getIntent().getExtras().get("message").toString());
            startActivity(jump);
        }
        nav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.search:
                        main_frag = new Search();
                        break;
                    case R.id.login:
                        startActivity(new Intent(nav.getContext(), LoginActivity.class));
                        break;
                    default:
                        main_frag = new Homepage();
                        break;

                }
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fakecontainer_view, main_frag)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
                return true;
            }
        });
    }
}
