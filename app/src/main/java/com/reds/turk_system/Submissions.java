package com.reds.turk_system;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class Submissions extends AppCompatActivity { // This Activity Displays all the project Submissions a client has.
    private ListView subproj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submissions);
        subproj = (ListView) findViewById(R.id.SubmittedList);

        new DataRetrieval(subproj, 15).execute(getIntent().getExtras().get("usernum").toString());

        subproj.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String[] data = adapterView.getItemAtPosition(i).toString().split(",");
                Intent jump = new Intent(subproj.getContext(), ClientSubmission.class);
                jump.putExtra("clientnum", data[2]);
                jump.putExtra("devnum", data[7]);
                jump.putExtra("projid", data[1]);
                startActivity(jump);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
