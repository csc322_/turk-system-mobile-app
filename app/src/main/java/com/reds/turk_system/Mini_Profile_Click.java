package com.reds.turk_system;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

/**
 * Created by Ronny on 12/1/2017.
 */

public class Mini_Profile_Click implements AdapterView.OnItemClickListener { //a clicker listener when a profile item is clicked.

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String[] data = ((String) adapterView.getItemAtPosition(i)).split(",");
        Intent jump = new Intent(view.getContext(), Mini_Profile_Activity.class); // presents the mini_profile_activity.
        jump.putExtra("userdata", data);
        view.getContext().startActivity(jump);


    }
}
