package com.reds.turk_system;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Ronny on 11/19/2017.
 */

public class Request_item extends BaseAdapter { // an item that belongs in the request list.
    private Context cont;
    private String[] data;

    private LayoutInflater inflater;
    private TextView name, from, to, type, val;
    private Button accept, reject;
    private ImageView prof;

    public Request_item(Context cont, String[] data) {
        this.cont = cont;
        this.data = data;
        this.inflater = (LayoutInflater) cont.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int i) {
        return data[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = view;
        if (vi == null)
            vi = inflater.inflate(R.layout.request_row, null);
        from = (TextView) vi.findViewById(R.id.from);
        name = (TextView) vi.findViewById(R.id.name);
        type = (TextView) vi.findViewById(R.id.typer);
        val = (TextView) vi.findViewById(R.id.value);
        accept = (Button) vi.findViewById(R.id.Accept);
        reject = (Button) vi.findViewById(R.id.Reject);
        prof = (ImageView) vi.findViewById(R.id.requestpic);
        to = (TextView) vi.findViewById(R.id.to_user);
        final String[] temp = data[i].split(",");
        from.setText("ID:" + temp[1]);
        name.setText(temp[3]);
        type.setText(temp[4]);
        to.setText("ID:" + temp[5]);
        switch (temp[2]) {
            case "pay":
                prof.setVisibility(View.GONE);
                type.setVisibility(View.GONE);
                val.setText("project ID:" + temp[6]);
                accept.setText("Pay");
                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DataSender(accept.getContext(), 9).execute(temp[6], temp[1], temp[5]);
                        new DataSender(accept.getContext(), 11).execute(temp[0]);
                        accept.setVisibility(View.GONE);
                        reject.setVisibility(View.GONE);
                    }
                });
                reject.setText("Cancel");
                reject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DataSender(accept.getContext(), 11).execute(temp[0]);
                        accept.setVisibility(View.GONE);
                        reject.setVisibility(View.GONE);
                    }
                });
                break;
            case "auth":
                val.setVisibility(View.GONE);
                to.setVisibility(View.GONE);
                name.setText("Request Authetication");
                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DataRetrieval(7).execute(from.getText().toString().replace("ID:", ""), type.getText().toString());
                        accept.setVisibility(View.GONE);
                        reject.setVisibility(View.GONE);
                        Toast.makeText(accept.getContext(), "User Accepted", Toast.LENGTH_LONG).show();
                        new DataSender(accept.getContext(), 11).execute(temp[0]);
                    }
                });
                reject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DataRetrieval(8).execute(from.getText().toString().replace("ID:", ""));
                        accept.setVisibility(View.GONE);
                        reject.setVisibility(View.GONE);
                        Toast.makeText(accept.getContext(), "User Rejected", Toast.LENGTH_LONG).show();
                        new DataSender(accept.getContext(), 11).execute(temp[0]);
                    }
                });
                break;
            case "disp":
                val.setVisibility(View.GONE);
                to.setVisibility(View.GONE);
                type.setVisibility(View.GONE);
                prof.setVisibility(View.GONE);
                accept.setText("Remove");
                reject.setText("cancel");

                break;
            case "black":
                val.setVisibility(View.GONE);
                to.setVisibility(View.GONE);
                type.setVisibility(View.GONE);
                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DataSender(accept.getContext(), 13).execute(temp[1]);
                        accept.setVisibility(View.GONE);
                        reject.setVisibility(View.GONE);
                        new DataSender(accept.getContext(), 11).execute(temp[0]);
                    }
                });
                reject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        accept.setVisibility(View.GONE);
                        reject.setVisibility(View.GONE);
                        new DataSender(accept.getContext(), 11).execute(temp[0]);
                    }
                });

                break;
            case "quit":
                val.setVisibility(View.GONE);
                to.setVisibility(View.GONE);
                type.setVisibility(View.GONE);
                name.setText("Wants to Quit");
                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DataSender(accept.getContext(), 12).execute(temp[1]);
                        accept.setVisibility(View.GONE);
                        reject.setVisibility(View.GONE);

                    }
                });
                reject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DataSender(accept.getContext(), 11).execute(temp[0]);
                        accept.setVisibility(View.GONE);
                        reject.setVisibility(View.GONE);
                    }
                });
        }


        return vi;
    }
}
