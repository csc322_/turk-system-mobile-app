package com.reds.turk_system;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


import java.net.URL;


import java.net.HttpURLConnection;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Ronny on 11/3/2017.
 */

public class DataRetrieval extends AsyncTask { // This class is in charge of Retrieving Data from the DataBase
    private EditText username;
    private EditText password;
    private Context context;
    private ProgressBar bar;
    private ListView list;
    private int type;
    private String data;
    private GridView grid;
    private RecyclerView relist;
    private View[] views;
    private TextView temp1, temp2;
    private static final String MAIN_URL = "http://69.202.153.170:1030/";
    private URL_Connection main_connection;
    private String link, usernum, type2;
    private String[] parsedData;

    protected DataRetrieval(TextView nc, TextView nd) {
        temp1 = nc;
        temp2 = nd;
        type = 17;
    }

    protected DataRetrieval(Context cox, EditText user, EditText pass, ProgressBar bar) {
        this.context = cox;
        this.username = user;
        this.password = pass;
        this.bar = bar;
        type = 1;
    }

    protected DataRetrieval(Context cox) {
        this.context = cox;
        this.type = 2;
    }

    protected DataRetrieval(ListView list, int type) {
        this.context = list.getContext();
        this.list = list;
        this.type = type;
    }

    protected DataRetrieval(GridView grid) {
        this.context = grid.getContext();
        this.grid = grid;
        this.type = 6;
    }

    protected DataRetrieval(GridView grid, int type) {
        this.context = grid.getContext();
        this.grid = grid;
        this.type = type;
    }

    protected DataRetrieval(int type) {
        this.type = type;

    }

    protected DataRetrieval(RecyclerView relist, int type) {
        this.context = relist.getContext();
        this.relist = relist;
        this.type = type;
    }

    protected DataRetrieval(View[] views, int type) {
        this.context = views[0].getContext();
        this.views = views;
        this.type = 12;

    }


    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        switch (type) {
            case 1:
                String userne = (String) objects[0];
                String passw = (String) objects[1];
                link = MAIN_URL + "login.php?username=" + userne + "&password=" + passw;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 3:
                usernum = (String) objects[0];
                link = MAIN_URL + "getprojects.php?usernum=" + usernum;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 4:
                usernum = (String) objects[0];
                link = MAIN_URL + "getbids.php?usernum=" + usernum;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 5:

                String keyword = (String) objects[0];
                link = MAIN_URL + "gatherinfo.php?keyword=" + keyword;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 6:
                type2 = (String) objects[0];
                link = MAIN_URL + "getrequest.php?type=" + type;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 7:
                usernum = (String) objects[0];
                type2 = (String) objects[1];
                link = MAIN_URL + "acceptuser.php?usernum=" + usernum + "&type=" + type2;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 8:
                usernum = (String) objects[0];
                link = MAIN_URL + "rejectuser.php?usernum=" + usernum;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 9:
                String link = MAIN_URL + "gettopdev.php";
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;

            case 10:
                link = MAIN_URL + "gettopclient.php";
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 11:
                link = MAIN_URL + "getendprojs.php";
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;


            case 13:
                String proj_id = (String) objects[0];
                link = MAIN_URL + "getprojbids.php?projid=" + proj_id;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 14:
                usernum = (String) objects[0];
                link = MAIN_URL + "getdevprojects.php?usernum=" + usernum;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 15:
                usernum = (String) objects[0];
                link = MAIN_URL + "getsubmitted.php?usernum=" + usernum;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 16:
                usernum = (String) objects[0];
                link = MAIN_URL + "getratings.php?usernum=" + usernum;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 17:
                link = MAIN_URL + "getstats.php";
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 18:
                link = MAIN_URL + "getwarnings.php";
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 19:
                String user_num = (String) objects[0];
                link = MAIN_URL + "getuserwarnings.php?usernum=" + user_num;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;

        }
        return "unsuccessful";
    }

    @Override
    protected void onPostExecute(Object o) {
        switch (type) {
            case 1:
                // change layout and call an activity if successful.
                parsedData = o.toString().split(",");
                ArrayList<String> data = new ArrayList<String>();
                Collections.addAll(data, parsedData);
                if (parsedData[0].compareTo("true") == 0) {
                    username.setText("success");
                    bar.setVisibility(View.GONE);
                    Intent jump = new Intent(context, Mainpage.class);
                    jump.putStringArrayListExtra("data1", data);
                    context.startActivity(jump);


                } else {
                    bar.setVisibility(View.GONE);
                    username.setText("no");
                }
                break;
            case 3:
                parsedData = o.toString().split("\n");
                if (parsedData[0].compareTo("false") != 0) {
                    list.setAdapter(new Project_Item(context, parsedData));
                } else {
                    Toast.makeText(context, "no Projects", Toast.LENGTH_SHORT).show();
                }
                break;
            case 4:
                parsedData = o.toString().split("\n");
                if (parsedData[0].compareTo("false") != 0) {
                    list.setAdapter(new Project_Item(context, parsedData));
                } else {
                    Toast.makeText(context, "no Bids", Toast.LENGTH_SHORT).show();
                }
                break;
            case 5:
                parsedData = o.toString().split("\n");
                if (parsedData[0].compareTo("false") != 0) {
                    list.setAdapter(new StandardListItem(context, parsedData));
                } else {
                    Toast.makeText(context, "No Results", Toast.LENGTH_SHORT).show();
                    list.setAdapter(null);
                }

                break;
            case 6:
                parsedData = o.toString().split("\n");
                if (parsedData[0].compareTo("false") != 0) {
                    grid.setAdapter(new Request_item(context, parsedData));
                } else {
                    Toast.makeText(context, "No Request", Toast.LENGTH_SHORT).show();
                }
                break;
            case 7:
                break;
            case 8:
                break;
            case 9:
                parsedData = o.toString().split("\n");
                if (parsedData[0].compareTo("false") != 0) {
                    relist.setAdapter(new Item_Profile(parsedData));
                } else {
                    Toast.makeText(context, "No Request", Toast.LENGTH_SHORT).show();
                }
                break;
            case 10:
                parsedData = o.toString().split("\n");
                if (parsedData[0].compareTo("false") != 0) {
                    relist.setAdapter(new Item_Profile(parsedData));
                } else {
                    Toast.makeText(context, "No Request", Toast.LENGTH_SHORT).show();
                }
                break;
            case 11:
                parsedData = o.toString().split("\n");
                if (parsedData[0].compareTo("false") != 0) {
                    list.setAdapter(new Project_Item(context, parsedData));
                } else {
                    Toast.makeText(context, "no Projects", Toast.LENGTH_SHORT).show();
                }
                break;
            case 13:
                parsedData = o.toString().split("\n");
                if (parsedData[0].compareTo("false") != 0) {
                    list.setAdapter(new Bid_item(context, parsedData));
                } else {
                    Toast.makeText(context, "no Bids", Toast.LENGTH_SHORT).show();
                }
                break;
            case 14:
                parsedData = o.toString().split("\n");
                if (parsedData[0].compareTo("false") != 0) {
                    list.setAdapter(new Project_Item(context, parsedData));
                } else {
                    Toast.makeText(context, "no Projects", Toast.LENGTH_SHORT).show();
                }
                break;
            case 15:
                parsedData = o.toString().split("\n");
                if (parsedData[0].compareTo("false") != 0) {
                    list.setAdapter(new Project_Item(context, parsedData));
                } else {
                    Toast.makeText(context, "no Projects", Toast.LENGTH_SHORT).show();
                }
                break;
            case 16:
                parsedData = o.toString().split("\n");
                if (parsedData[0].compareTo("false") != 0) {
                    list.setAdapter(new Rating_Item(context, parsedData));
                } else {
                    Toast.makeText(context, "no ratings", Toast.LENGTH_SHORT).show();
                }
                break;
            case 17:
                parsedData = o.toString().split(",");
                temp1.setText("Number of Clients:" + parsedData[1]);
                temp2.setText("Number of Developers:" + parsedData[0]);
                break;
            case 18:
                parsedData = o.toString().split("\n");
                if (parsedData[0].compareTo("false") != 0) {
                    grid.setAdapter(new Warning_Item(context, parsedData));
                } else {
                    Toast.makeText(context, "no warnings", Toast.LENGTH_SHORT).show();
                }
                break;
            case 19:
                parsedData = o.toString().split("\n");
                if (parsedData[0].compareTo("false") != 0) {
                    list.setAdapter(new User_Warning_item(context, parsedData));
                } else {
                    Toast.makeText(context, "no warnings", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
