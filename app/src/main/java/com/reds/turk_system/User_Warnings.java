package com.reds.turk_system;

import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class User_Warnings extends AppCompatActivity { // the screen shown to the user that holds all their warnings.
    private ListView warn_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user__warnings);
        warn_list = (ListView) findViewById(R.id.warning_list);
        new DataRetrieval(warn_list, 19).execute(getIntent().getExtras().get("usernum"));

    }
}
