package com.reds.turk_system;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Warnings extends Fragment { // The Warning Fragment displays all the warnings in the system to the SU.
    private GridView grid;

    public Warnings() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_warnings, container, false);
        grid = (GridView) root.findViewById(R.id.warning_grid);
        new DataRetrieval(grid, 18).execute();
        return root;
    }

}
