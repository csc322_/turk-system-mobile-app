package com.reds.turk_system;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Blacklist extends Fragment { // This is the screen that is presented to the SU when they click on the blacklist tab in the app
    private GridView grid;

    public Blacklist() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_blacklist, container, false);
        grid = (GridView) root.findViewById(R.id.black_grid);
        new DataRetrieval(grid).execute("black");
        return root;
    }

}
