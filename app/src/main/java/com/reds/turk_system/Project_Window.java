package com.reds.turk_system;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class Project_Window extends AppCompatActivity { //Screen that previews all project information.
    private TextView projname, projdesc,eob,due,devid,bidval;
    private ListView bids;
    private Button bidbut;
    private String[] projdata;
    private ArrayList<String> userdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_window);
        //setup
        projname = (TextView) findViewById(R.id.proj_title);
        projdesc = (TextView) findViewById(R.id.proj_Desc);
        bids = (ListView) findViewById(R.id.BidderList);
        eob = (TextView) findViewById(R.id.EOB);
        due = (TextView) findViewById(R.id.due_Date);
        devid =(TextView) findViewById(R.id.devid);
        bidval = (TextView) findViewById(R.id.bidval);
        projdata = getIntent().getExtras().getStringArray("projdata");
        userdata = getIntent().getStringArrayListExtra("userdata");
        bidbut = (Button) findViewById(R.id.PlaceBid);
        projname.setText(projdata[3]);
        projdesc.setText(projdata[4]);
        devid.setText(projdata[7]);
        eob.setText(projdata[9]);
        due.setText(projdata[5]);
        bidval.setText(projdata[10]);
        //check the state of the project
        switch (projdata[8]) {
            case "0": // open for bid
                if (userdata != null) {
                    switch (userdata.get(3)) { // check type of user
                        case "dev":
                            bidbut.setVisibility(View.VISIBLE);
                            bidbut.setText("BID");
                            bidbut.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent jump = new Intent(bidbut.getContext(), PlaceBid.class);
                                    jump.putExtra("projdata", projdata);
                                    jump.putExtra("usernum", userdata.get(1));
                                    startActivity(jump);
                                }
                            });
                            break;
                        default:
                            bidbut.setVisibility(View.GONE);
                            break;

                    }
                    new DataRetrieval(bids, 13).execute(projdata[1]);
                } else {
                    new DataRetrieval(bids, 13).execute(projdata[1]);
                    bidbut.setVisibility(View.GONE);
                }
                break;
            case "1": // inprogress developer has been selected
                if (userdata != null) {
                    if (userdata.get(1).toString().compareTo(projdata[7]) == 0) { // check to see if the user is the current developer of the project.
                        bidbut.setText("Submit");
                        bids.setVisibility(View.GONE);
                        bidbut.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) { // submission button on click listener

                                Intent jump = new Intent(bidbut.getContext(),Dev_Submission.class);
                                jump.putExtra("devnum",userdata.get(1));
                                jump.putExtra("clientnum",projdata[2]);
                                jump.putExtra("projid",projdata[1]);
                                finish();
                                startActivity(jump);
                            }
                        });
                    } else {
                        bidbut.setVisibility(View.GONE);
                        bids.setVisibility(View.GONE);
                    }
                } else {
                    bidbut.setVisibility(View.GONE);
                    bids.setVisibility(View.GONE);
                }
                break;
            case "2": // over due
                bidbut.setVisibility(View.GONE);
                bids.setVisibility(View.GONE);

                break;
            case "3": // submitted
                bidbut.setVisibility(View.GONE);
                bids.setVisibility(View.GONE);

                break;
            case "4": // completed
                bidbut.setVisibility(View.GONE);
                bids.setVisibility(View.GONE);
                break;
        }


    }
}
