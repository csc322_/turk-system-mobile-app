package com.reds.turk_system;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

public class Create_Proj extends AppCompatActivity {
    private TextView proj_name, proj_desc, proj_value;
    private DatePicker due_date, end_bid;
    private Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) { // This screen is presented to the client when they create a new project!
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_proj);
        proj_name = (TextView) findViewById(R.id.proj_name);
        proj_desc = (TextView) findViewById(R.id.Descrip);
        proj_value = (TextView) findViewById(R.id.value);
        due_date = (DatePicker) findViewById(R.id.datePicker);
        end_bid = (DatePicker) findViewById(R.id.bid_end);
        submit = (Button) findViewById(R.id.submit);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = proj_name.getText().toString();
                String desc = proj_desc.getText().toString();
                String value = proj_value.getText().toString();
                String date = formatDate(due_date.getYear(), due_date.getMonth() + 1, due_date.getDayOfMonth());
                String biddate = formatDate(end_bid.getYear(), end_bid.getMonth() + 1, end_bid.getDayOfMonth());
                String usernum = getIntent().getStringExtra("un");
                if (Float.parseFloat(value) < Float.parseFloat(getIntent().getExtras().get("bank").toString())) {
                    new DataSender(submit.getContext(), 2).execute(usernum, name, desc, date, value, biddate);
                } else {
                    Toast.makeText(submit.getContext(), "not enough money in the bank", Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    public String formatDate(int year, int month, int day) {
        String date = Integer.toString(year);
        if (month < 10) {
            date += ("-0" + month);
        } else {
            date += ("-" + month);
        }
        if (day < 10) {
            date += ("-0" + day);
        } else {
            date += ("-" + day);
        }
        return date;


    }
}
