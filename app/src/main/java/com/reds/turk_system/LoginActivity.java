package com.reds.turk_system;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    private EditText mEmailView;
    private EditText mPasswordView;
    private DataRetrieval temp;
    private ProgressBar prog;
    private Button mRegisterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        prog = (ProgressBar) findViewById(R.id.login_progress);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    temp = new DataRetrieval(mPasswordView.getContext(), mEmailView, mPasswordView, prog);
                    prog.setVisibility(View.VISIBLE);
                    temp.execute(mEmailView.getText().toString(), mPasswordView.getText().toString());

                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                temp = new DataRetrieval(mPasswordView.getContext(), mEmailView, mPasswordView, prog);
                prog.setVisibility(View.VISIBLE);
                temp.execute(mEmailView.getText().toString(), mPasswordView.getText().toString());

            }
        });
        mRegisterButton = (Button) findViewById(R.id.Register);
        mRegisterButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mRegisterButton.getContext(), Registration.class));
            }
        });


    }


}