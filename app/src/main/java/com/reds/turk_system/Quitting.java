package com.reds.turk_system;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Quitting extends AppCompatActivity { // A screen presented to all users when they want to quit the system.
    private EditText reason;
    private Button quit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quitting);
        reason = (EditText) findViewById(R.id.Reason);
        quit = (Button) findViewById(R.id.quitter);

        quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { // quit submission button on click listener
                if (reason.getText().toString().compareTo("") == 0) {
                    reason.setError("you need to give a reason");
                } else {
                    new DataSender(quit.getContext(), 10).execute("", getIntent().getExtras().get("usernum"), "", reason.getText().toString(), "quit");
                    finish();
                }
            }
        });


    }
}
