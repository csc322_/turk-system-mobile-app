package com.reds.turk_system;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Ronny on 11/15/2017.
 */

public class Bid_item extends BaseAdapter {
    Context context;
    String[] data;
    String[] temp;
    private TextView projname, projid, datetext, bidamount;
    private Button bidchoice;
    private static LayoutInflater inflater;

    public Bid_item(Context context, String[] data) { // This is a bidding item. Essentialy when you see the list of bidders the individual items in that list will take this form.
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int i) {
        return data[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = view;
        if (vi == null)
            vi = inflater.inflate(R.layout.bid_row, null);
        projname = (TextView) vi.findViewById(R.id.textfield);
        projid = (TextView) vi.findViewById(R.id.texty);
        datetext = (TextView) vi.findViewById(R.id.datetext);
        bidamount = (TextView) vi.findViewById(R.id.Bidamount);
        bidchoice = (Button) vi.findViewById(R.id.SelectButton);
        temp = data[i].split(",");
        final String user_num;
        String proj_detail;
        if ((((Activity) vi.getContext()).getIntent().getExtras().getStringArrayList("userdata") != null)) {
            user_num = ((Activity) vi.getContext()).getIntent().getExtras().getStringArrayList("userdata").get(1);
            proj_detail = ((Activity) vi.getContext()).getIntent().getExtras().getStringArray("projdata")[2];
        } else {
            user_num = temp[5];
            proj_detail = "0";
        }

        projname.setText("User_ID: " + temp[5]);
        projid.setText("Project ID: " + temp[2]);
        datetext.setText(temp[4]);
        bidamount.setText("$:" + temp[3]);
        if (user_num.compareTo(proj_detail) != 0) {
            bidchoice.setVisibility(View.GONE);
        } else {
            bidchoice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String val = Float.parseFloat(temp[3]) + "";
                    if (Float.parseFloat(val) < Float.parseFloat(((Activity) bidchoice.getContext()).getIntent().getExtras().getStringArrayList("userdata").get(8))) {
                        new DataSender(bidchoice.getContext(), 4).execute(temp[2], temp[5], user_num, val);
                    } else {
                        Toast.makeText(bidchoice.getContext(), "you need more money pal!", Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }
        return vi;
    }
}
