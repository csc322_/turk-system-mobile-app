package com.reds.turk_system;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class Settings extends AppCompatActivity { // The settings screen
    private ListView s_list;
    private ArrayList<String> data;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        s_list = (ListView) findViewById(R.id.Setting_list);
        data = getIntent().getStringArrayListExtra("metadata");
        s_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                switch (i) {

                    case 0: // Bio wants to be edited
                        Intent bio_data = new Intent(s_list.getContext(), Edit_Bio.class);
                        bio_data.putExtra("bio", new String[]{data.get(6), data.get(1)});
                        startActivity(bio_data);
                        break;
                    case 1://Interest wants to be edited
                        Intent interest_data = new Intent(s_list.getContext(), Edit_Interest.class);
                        interest_data.putExtra("interest", new String[]{data.get(7), data.get(1)});
                        startActivity(interest_data);
                        break;
                    case 2: // profile pic wants to be edited
                        Intent change_pic = new Intent(s_list.getContext(), Edit_Picture.class);
                        change_pic.putExtra("usernum", data.get(1));
                        startActivity(change_pic);
                        break;
                    case 3: // Shows user a List of ratings towards them.
                        Intent rating_list = new Intent(s_list.getContext(), Ratings.class);
                        rating_list.putExtra("usernum", data.get(1));
                        startActivity(rating_list);
                        break;
                    case 4: // Shows users all projects that have been submitted to them.
                        Intent submissions = new Intent(s_list.getContext(), Submissions.class);
                        submissions.putExtra("usernum", data.get(1));
                        startActivity(submissions);
                        break;
                    case 5://bank info wants to be edited
                        Intent banking = new Intent(s_list.getContext(), Banker.class);
                        banking.putExtra("usernum", data.get(1));
                        startActivity(banking);
                        break;
                    case 6://show user a list of warnings
                        Intent warnings = new Intent(s_list.getContext(), User_Warnings.class);
                        warnings.putExtra("usernum", data.get(1));
                        warnings.putExtra("usertype", data.get(3));
                        startActivity(warnings);
                        break;
                    case 7://send super user messages
                        Intent gen = new Intent(s_list.getContext(), General.class);
                        gen.putExtra("usernum", data.get(1));
                        startActivity(gen);
                    case 8:// quit
                        Intent quitting = new Intent(s_list.getContext(), Quitting.class);
                        quitting.putExtra("usernum", data.get(1));
                        startActivity(quitting);
                        break;
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
