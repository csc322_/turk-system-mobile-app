package com.reds.turk_system;

import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Ronny on 11/29/2017.
 */

public class SearchClickDecider implements AdapterView.OnItemClickListener { // a click listener that decides what to present on the screen depending on the item clicked in the search list.

    private ArrayList<String> userdata;

    public SearchClickDecider(ArrayList<String> userdata) {
        this.userdata = userdata;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String type = ((TextView) view.findViewById(R.id.type_id)).getText().toString();
        switch (type) {
            case "Project":
                new Project_Viewer(userdata).onItemClick(adapterView, view, i, l);
                break;
            case "User":
                new Mini_Profile_Click().onItemClick(adapterView, view, i, l);
                break;
            default:
                break;

        }
    }
}
