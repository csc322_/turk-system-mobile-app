package com.reds.turk_system;


import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Authorize extends Fragment { // Presents the SU with a screen that shows all user request to join the system.
    private GridView grid;

    public Authorize() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_authorize, container, false);
        grid = (GridView) root.findViewById(R.id.auth_grid);
        new DataRetrieval(grid).execute("auth");
        return root;

    }

}
