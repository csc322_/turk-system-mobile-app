package com.reds.turk_system;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Ronny on 11/13/2017.
 */

public class Project_Item extends BaseAdapter { // a project item that is shown in a project list.
    Context context;
    String[] data;
    private static LayoutInflater inflater = null;
    public Project_Item(Context cont, String[] data ) {

        this.data = data;
        this.context= cont;
        inflater = (LayoutInflater) cont.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public String getItem(int i) {
        return data[i] ;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = view;
        if (vi == null)
            vi = inflater.inflate(R.layout.proj_row, null);
        TextView projname = (TextView) vi.findViewById(R.id.textfield);
        TextView projid = (TextView) vi.findViewById(R.id.texty);
        TextView datetext= (TextView) vi.findViewById(R.id.datetext);
        TextView progress = (TextView) vi.findViewById(R.id.Status);

        String temp[] = data[i].split(",");
        projname.setText("Name: "+temp[3]);
        projid.setText("Project ID: "+temp[1]);
        datetext.setText(temp[5]);
        switch (temp[8]){
            case "0":
                progress.setText("Open for Bid");
                break;
            case "1":
                progress.setText("In Progress");
                break;
            case "2":
                progress.setText("Over Due");
                break;
            case "3":
                progress.setText("Submitted");
                break;
            case "4":
                progress.setText("Completed");
                break;
        }
        return vi;

    }
}

