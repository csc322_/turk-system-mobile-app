package com.reds.turk_system;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

/**
 * Created by Ronny on 11/23/2017.
 */



public class Item_Profile extends RecyclerView.Adapter<Item_Profile.MyViewHolder> { // the items behind the top client/ dev lists
private String [] data;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView rate;
        private ImageView img;

        public MyViewHolder(View itemView) {
            super(itemView);

            this.rate = (TextView) itemView.findViewById(R.id.rate);
            this.img = (ImageView) itemView.findViewById(R.id.prof_image);
        }
    }

    public Item_Profile(String[] data) {
        this.data = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_item,parent,false);

        return new MyViewHolder(root);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String [] temp  = data[position].split(",");
        holder.rate.setText(temp[2]);
        Glide.with(holder.rate.getContext()).load(temp[1]).into(holder.img);
    }

    @Override
    public int getItemCount() {
        return data.length;
    }
}
