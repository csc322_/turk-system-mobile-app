package com.reds.turk_system;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

public class Dev_Submission extends AppCompatActivity { // This screen is shown to the developers when they are submitting a project.
    private EditText desc;
    private RatingBar rate;
    private Button submitrate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submission);
        desc = (EditText) findViewById(R.id.Description);
        rate = (RatingBar) findViewById(R.id.ratingBar);
        submitrate = (Button) findViewById(R.id.Submitrate);
        submitrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rate.getRating() < 3 && desc.getText().toString().compareTo("") == 0) {
                    desc.setError("Explain Your Rating Please");
                } else {
                    new DataSender(submitrate.getContext(), 6).execute(getIntent().getExtras().get("devnum").toString(), getIntent().getExtras().get("clientnum"), rate.getRating() + "", desc.getText().toString());
                    new DataSender(submitrate.getContext(), 7).execute(getIntent().getExtras().get("projid"));
                }

            }
        });
    }
}
