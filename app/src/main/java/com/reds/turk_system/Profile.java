package com.reds.turk_system;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class Profile extends Fragment { // The main profile tab that shows the user their own profile.
    TextView name, bio, rating, interest, bank;
    ListView proj_list;
    ArrayList<String> data;
    ImageView prof;
    Button settings;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        //setup
        name = (TextView) root.findViewById(R.id.Name);
        bio = (TextView) root.findViewById(R.id.Bio_Desc);
        interest = (TextView) root.findViewById(R.id.interests);
        proj_list = (ListView) root.findViewById(R.id.proj_list);
        rating = (TextView) root.findViewById(R.id.rating);
        bank = (TextView) root.findViewById(R.id.bank);
        settings = (Button) root.findViewById(R.id.settings);
        prof = (ImageView) root.findViewById(R.id.profilepic);
        data = getArguments().getStringArrayList("data");
        name.setText(data.get(2));
        Glide.with(getContext()).load(data.get(5)).into(prof);
        bio.setText(data.get(6));
        interest.setText(data.get(7));
        bank.setText(data.get(8));
        rating.setText(data.get(9));
        //check type of user.
        if (data.get(3).compareTo("dev") == 0) {
            new DataRetrieval(proj_list, 14).execute(data.get(1));
        } else {
            new DataRetrieval(proj_list, 3).execute(data.get(1));
        }

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {// click listener for setting button.
                Intent act = new Intent(settings.getContext(), Settings.class);
                act.putStringArrayListExtra("metadata", data);
                startActivity(act);
            }
        });
        proj_list.setOnItemClickListener(new Project_Viewer(data));

        return root;
    }


}
