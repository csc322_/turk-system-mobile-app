package com.reds.turk_system;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class Search extends Fragment { // The search screen.
    EditText searchbar;
    ListView projlist;
    Button searchbut;
    int menutype;

    public Search() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_search, container, false);
        searchbar = (EditText) root.findViewById(R.id.SearchBar);
        projlist = (ListView) root.findViewById(R.id.projlist);
        searchbut = (Button) root.findViewById(R.id.Searchbut);
        searchbut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { // The search button listener
                if (searchbar.getText().toString().isEmpty()) { // make sure search bar is not empty
                    Toast.makeText(searchbar.getContext(), "Input Text in Search Bar ", Toast.LENGTH_LONG).show();
                    return;
                }

                InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                new DataRetrieval(projlist, 5).execute(searchbar.getText().toString()); // get all projects and users that match the search term.
                if (getActivity().getLocalClassName().compareTo("FakeMainPage") == 0) {
                    projlist.setOnItemClickListener(new SearchClickDecider(null));
                } else {
                    projlist.setOnItemClickListener(new SearchClickDecider(getArguments().getStringArrayList("data")));
                }
                return;


            }
        });


        return root;
    }

}
