package com.reds.turk_system;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Ronny on 12/10/2017.
 */

public class Warning_Item extends BaseAdapter { // an item that belongs in the SU warnings list
    private Context cont;
    private String[] data, temp;

    private LayoutInflater inflater;
    private EditText answer;

    public Warning_Item(Context cont, String[] data) {
        this.cont = cont;
        this.data = data;
        this.inflater = (LayoutInflater) cont.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int i) {
        return data[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = view;
        if (vi == null)
            vi = inflater.inflate(R.layout.warning_row, null);
        temp = data[i].split(",");
        TextView user_id = (TextView) vi.findViewById(R.id.user_num);
        TextView comment = (TextView) vi.findViewById(R.id.Comment);
        answer = (EditText) vi.findViewById(R.id.response);
        Button accept = (Button) vi.findViewById(R.id.accetp_dis);
        Button reject = (Button) vi.findViewById(R.id.reject_dis);
        Button respond = (Button) vi.findViewById(R.id.responde_to);
        TextView type = (TextView) vi.findViewById(R.id.type);
        type.setText(temp[3]);
        user_id.setText(temp[1]);
        comment.setText(temp[2]);
        switch (temp[3]) {
            case "warn":
                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DataSender(view.getContext(), 15).execute(temp[1]);
                        new DataSender(view.getContext(), 16).execute(temp[0]);

                    }
                });
                reject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DataSender(view.getContext(), 16).execute(temp[0]);
                    }
                });
                respond.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        new DataSender(view.getContext(), 17).execute(temp[0], "SU:" + answer.getText() + "");
                    }
                });
                break;
            case "dis":
                respond.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DataSender(view.getContext(), 17).execute(temp[0], "SU:" + answer.getText() + "");
                    }
                });
                accept.setVisibility(View.GONE);
                reject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DataSender(view.getContext(), 16).execute(temp[0]);
                    }
                });
            case "rate":

        }


        return vi;

    }
}
