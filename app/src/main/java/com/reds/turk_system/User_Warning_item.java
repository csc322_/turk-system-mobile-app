package com.reds.turk_system;

import android.app.LauncherActivity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Ronny on 12/10/2017.
 */

public class User_Warning_item extends BaseAdapter { // the item found in the warnings list
    private Context cont;
    private String[] data, temp;
    private TextView comment, user;
    private LayoutInflater inflater;
    private EditText answer;
    private Button respond, accept, reject;

    public User_Warning_item(Context cont, String[] data) {
        this.cont = cont;
        this.data = data;
        this.inflater = (LayoutInflater) cont.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int i) {
        return data[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = view;
        if (vi == null)
            vi = inflater.inflate(R.layout.warning_row, null);
        temp = data[i].split(",");
        user = (TextView) vi.findViewById(R.id.user_num);
        comment = (TextView) vi.findViewById(R.id.Comment);
        answer = (EditText) vi.findViewById(R.id.response);
        respond = (Button) vi.findViewById(R.id.responde_to);
        accept = (Button) vi.findViewById(R.id.accetp_dis);
        reject = (Button) vi.findViewById(R.id.reject_dis);
        user.setText(temp[1]);
        comment.setText(temp[2]);
        respond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DataSender(view.getContext(), 17).execute(temp[0], "User:" + answer.getText() + "");
            }
        });
        accept.setVisibility(View.GONE);
        reject.setVisibility(View.GONE);

        return vi;

    }
}
