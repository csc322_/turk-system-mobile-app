package com.reds.turk_system;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;


/**
 * Created by Ronny on 11/28/2017.
 */

public class Project_Viewer implements AdapterView.OnItemClickListener { // a click listener for clicking on a project item.
    private ArrayList<String> userdata;

    public Project_Viewer(ArrayList<String> userdata) {
        this.userdata = userdata;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String[] data = ((String) adapterView.getItemAtPosition(i)).split(",");
        Intent jump = new Intent(adapterView.getContext(), Project_Window.class);// presents the user with a screen that previews all project information.
        jump.putExtra("projdata", data);

        if (userdata != null) {
            jump.putStringArrayListExtra("userdata", userdata);
        } else {
            jump.putStringArrayListExtra("userdata", null);
        }
        view.getContext().startActivity(jump);


    }
}
