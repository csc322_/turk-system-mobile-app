package com.reds.turk_system;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Ronny on 12/11/2017.
 */

public class URL_Connection { // controls the connection with the server
    private String LINK;
    private String data;

    public URL_Connection(String LINK) {
        this.LINK = LINK;
        link_up();
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public String link_up() {
        try {
            URL url = new URL(LINK);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            InputStream stream = conn.getInputStream();
            data = convertStreamToString(stream);
            return data;
        } catch (Exception e) {
            Log.d("tag", "Exception: " + e.getMessage());
            return "unsuccesful";
        }
    }

}
