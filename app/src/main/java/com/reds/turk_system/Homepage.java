package com.reds.turk_system;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v7.widget.LinearLayoutManager;


/**
 * A simple {@link Fragment} subclass.
 */
public class Homepage extends Fragment { // This screen is presented to registered users when they log into the app.
    private RecyclerView topc, topd;
    private ListView ending;
    private TextView nc, nd;

    public Homepage() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = (View) inflater.inflate(R.layout.fragment_homepage, container, false);
        topd = (RecyclerView) root.findViewById(R.id.Dev_list);
        topc = (RecyclerView) root.findViewById(R.id.Client_list);
        ending = (ListView) root.findViewById(R.id.Project_grid);
        nc = (TextView) root.findViewById(R.id.clientnum);
        nd = (TextView) root.findViewById(R.id.devnum);
        LinearLayoutManager llm = new LinearLayoutManager(root.getContext());
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        topd.setLayoutManager(llm);
        LinearLayoutManager ll = new LinearLayoutManager(root.getContext());
        ll.setOrientation(LinearLayoutManager.HORIZONTAL);
        topc.setLayoutManager(ll);

        new DataRetrieval(nc, nd).execute();
        new DataRetrieval(topd, 9).execute();
        new DataRetrieval(topc, 10).execute();
        new DataRetrieval(ending, 11).execute();
        if (getActivity().getLocalClassName().compareTo("FakeMainPage") == 0) {
            ending.setOnItemClickListener(new Project_Viewer(null));
        } else {
            ending.setOnItemClickListener(new Project_Viewer(getArguments().getStringArrayList("data")));
        }

        return root;
    }

}
