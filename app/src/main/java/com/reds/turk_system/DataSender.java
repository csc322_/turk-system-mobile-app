package com.reds.turk_system;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


import java.net.URL;


import java.net.HttpURLConnection;

import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;


/**
 * Created by Ronny on 11/25/2017.
 */

public class DataSender extends AsyncTask { // This class is in charge of sending data to the DataBase
    private String data = "bacon", link, projid, usernum;
    private int type;
    private Context cont;
    private static final String MAIN_URL = "http://69.202.153.170:1030/";
    private URL_Connection main_connection;

    public DataSender(Context cont, int type) {
        this.cont = cont;
        this.type = type;


    }

    protected DataSender(Context cont) {
        this.cont = cont;
        this.type = 3;
    }


    @Override
    protected Object doInBackground(Object[] objects) {
        switch (type) {
            case 0:
                String user_num = (String) objects[1];
                String bio = (String) objects[0];
                link = MAIN_URL + "updatebio.php?bio=Bio:" + bio + "&usernum=" + user_num;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;

            case 1:
                usernum = (String) objects[1];
                String interest = "Interest: " + (String) objects[0];
                link = MAIN_URL + "updateinterest.php?interest=" + interest + "&usernum=" + usernum;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 2:
                usernum = (String) objects[0];
                String projname = (String) objects[1];
                String projdesc = (String) objects[2];
                String projdate = (String) objects[3];
                String projvalue = (String) objects[4];
                String endbid = (String) objects[5];
                link = MAIN_URL + "uploadproj.php?usernum=" + usernum + "&title=" + projname + "&desc=" + projdesc + "&date=" + projdate + "&value=" + projvalue + "&endbid=" + endbid;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 3:
                String userne = (String) objects[0];
                String pass = (String) objects[1];
                String name = (String) objects[2];
                String type = (String) objects[3];
                link = MAIN_URL + "registercheck.php?username=" + userne;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                String[] temp = data.split("\n");
                if (temp[0].compareTo("false") == 0) {
                    return data;
                } else {
                    String link2 = MAIN_URL + "register.php?username=" + userne + "&password=" + pass + "&name=" + name + "&type=" + type;
                    main_connection = new URL_Connection(link2);
                    data = main_connection.link_up();
                    return data;
                }

            case 4:

                String proj_id = (String) objects[0];
                String dev_num = (String) objects[1];
                String client_num = (String) objects[2];
                String val = (String) objects[3];
                link = MAIN_URL + "updateproject.php?projid=" + proj_id + "&dev=" + dev_num + "&client=" + client_num + "&val=" + val;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;

            case 5:
                projid = (String) objects[1];
                usernum = (String) objects[0];
                String bidval = (String) objects[2];
                link = MAIN_URL + "placebid.php?usernum=" + usernum + "&projid=" + projid + "&value=" + bidval;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 6:
                String rate = (String) objects[2];
                String frm_num = (String) objects[0];
                String addr_num = (String) objects[1];
                String desc = (String) objects[3];

                link = MAIN_URL + "rated.php?frm=" + frm_num + "&addr=" + addr_num + "&rate=" + rate + "&desc=" + desc;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 7:
                projid = (String) objects[0];
                link = MAIN_URL + "submitproj.php?projid=" + projid;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 8:
                projid = (String) objects[0];
                link = MAIN_URL + "completeproj.php?projid=" + projid;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 9:
                projid = (String) objects[0];
                String usernum1 = (String) objects[1];
                String usernum2 = (String) objects[2];
                link = MAIN_URL + "sendcash.php?projid=" + projid + "&frm=" + usernum1 + "&addr=" + usernum2;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 10:
                projid = (String) objects[0];
                String usernum12 = (String) objects[1];
                String usernum22 = (String) objects[2];
                String desc1 = (String) objects[3];
                String type1 = (String) objects[4];
                link = MAIN_URL + "placerequest.php?projid=" + projid + "&frm=" + usernum12 + "&addr=" + usernum22 + "&desc=" + desc1 + "&type=" + type1;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 11:
                String requestid = (String) objects[0];
                link = MAIN_URL + "deleterequest.php?reqid=" + requestid;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 12:

                usernum = (String) objects[0];
                link = MAIN_URL + "erasehistory.php?usernum=" + usernum;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 13:
                usernum = (String) objects[0];
                link = MAIN_URL + "blacklist.php?usernum=" + usernum;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 14:
                usernum = (String) objects[0];
                String amount = (String) objects[1];
                link = MAIN_URL + "updatebank.php?usernum=" + usernum + "&amount=" + amount;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 15:
                usernum = (String) objects[0];
                link = MAIN_URL + "removewarning.php?usernum=" + usernum;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 16:
                String warn_id = (String) objects[0];
                link = MAIN_URL + "deletewarning.php?warnid=" + warn_id;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 17:
                String warn_id2 = (String) objects[0];
                String response = (String) objects[1];
                if (response.compareTo("") == 0) {
                    response = "n/a";
                }
                link = MAIN_URL + "respondwarning.php?warnid=" + warn_id2 + "&response=" + response;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
            case 18:
                usernum = (String) objects[0];
                String response2 = (String) objects[1];
                String type2 = (String) objects[2];
                if (response2.compareTo("") == 0) {
                    response = "n/a";
                }
                link = MAIN_URL + "senddispute.php?usernum=" + usernum + "&response=" + response2 + "&type=" + type2;
                main_connection = new URL_Connection(link);
                data = main_connection.link_up();
                return data;
        }


        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        switch (type) {
            case 0:
                String temp[] = o.toString().split("\n");
                if (temp[0].compareTo("false") == 0) {
                    Toast.makeText(cont, "Bio update failed try again please", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(cont, "Bio update successful ", Toast.LENGTH_SHORT).show();
                }
                break;
            case 1:
                String temp1[] = o.toString().split("\n");
                if (temp1[0].compareTo("false") == 0) {
                    Toast.makeText(cont, "Interest update failed try again please", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(cont, "Interest update successful ", Toast.LENGTH_SHORT).show();
                }
                break;
            case 2:
                String temp2[] = o.toString().split("\n");
                if (temp2[0].compareTo("false") != 0) {
                    Toast.makeText(cont, "project uploaded", Toast.LENGTH_SHORT).show();
                    ((Activity) cont).finish();
                } else {
                    Toast.makeText(cont, "Error try again later ", Toast.LENGTH_SHORT).show();
                }
                break;
            case 3:
                String temp3[] = o.toString().split("\n");
                if (temp3[0].compareTo("true") == 0) {
                    Toast.makeText(cont, "Registration complete Sign in", Toast.LENGTH_SHORT).show();
                    cont.startActivity(new Intent(cont, LoginActivity.class));
                } else {
                    Toast.makeText(cont, "User all ready exits", Toast.LENGTH_SHORT).show();
                }
                break;
            case 4:
                String temp4[] = o.toString().split("\n");
                if (temp4[0].compareTo("true") == 0) {
                    Toast.makeText(cont, "Developer Selected!", Toast.LENGTH_SHORT).show();
                    ((Activity) cont).finish();
                } else {
                    Toast.makeText(cont, "Error Selecting Developer Try Again", Toast.LENGTH_SHORT).show();
                }
                break;
            case 5:
                String temp5[] = o.toString().split("\n");
                if (temp5[0].compareTo("true") == 0) {
                    Toast.makeText(cont, "Bid Placed", Toast.LENGTH_SHORT).show();
                    ((Activity) cont).finish();
                } else {
                    Toast.makeText(cont, "Error Placing Bid Try Again", Toast.LENGTH_SHORT).show();
                }
                break;
            case 6:
                String temp6[] = o.toString().split("\n");
                if (temp6[0].compareTo("true") == 0) {
                    Toast.makeText(cont, "Rating Placed", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(cont, "Error Try Again", Toast.LENGTH_SHORT).show();
                }
                break;
            case 7:
                String temp7[] = o.toString().split("\n");
                if (temp7[0].compareTo("true") == 0) {
                    Toast.makeText(cont, "Project Complete", Toast.LENGTH_SHORT).show();
                    ((Activity) cont).finish();
                } else {
                    Toast.makeText(cont, "Error Try Again", Toast.LENGTH_SHORT).show();
                }
                break;

            case 8:
                String temp8[] = o.toString().split("\n");
                if (temp8[0].compareTo("true") == 0) {
                    Toast.makeText(cont, "Project Complete", Toast.LENGTH_SHORT).show();
                    ((Activity) cont).finish();
                } else {
                    Toast.makeText(cont, "Error Try Again", Toast.LENGTH_SHORT).show();
                }
                break;

            case 9:
                String temp9[] = o.toString().split("\n");
                if (temp9[0].compareTo("true") == 0) {
                    Toast.makeText(cont, "Money Sent", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(cont, "Error Try Again", Toast.LENGTH_SHORT).show();
                }
                break;

            case 10:
                String temp10[] = o.toString().split("\n");
                if (temp10[0].compareTo("true") == 0) {
                    Toast.makeText(cont, "Request Placed", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(cont, "Error Try Again", Toast.LENGTH_SHORT).show();
                }
                break;
            case 11:
                String temp11[] = o.toString().split("\n");
                if (temp11[0].compareTo("true") == 0) {
                    Toast.makeText(cont, "Request Removed", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(cont, "Error Try Again", Toast.LENGTH_SHORT).show();
                }
                break;
            case 12:

                break;
            case 13:

                break;
            case 14:
                break;
            case 15:
                String temp15[] = o.toString().split("\n");
                if (temp15[0].compareTo("true") == 0) {
                    Toast.makeText(cont, "Warning Removed from user", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(cont, "Error Try Again", Toast.LENGTH_SHORT).show();
                }
                break;
            case 16:
                String temp16[] = o.toString().split("\n");
                if (temp16[0].compareTo("true") == 0) {
                    Toast.makeText(cont, "warning removed ", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(cont, "Error Try Again", Toast.LENGTH_SHORT).show();
                }
                break;
            case 17:
                String temp17[] = o.toString().split("\n");
                if (temp17[0].compareTo("true") == 0) {
                    Toast.makeText(cont, "Response sent", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(cont, "Error Try Again", Toast.LENGTH_SHORT).show();
                }
                break;
            case 18:

                break;
        }
    }
}
