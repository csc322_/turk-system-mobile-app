package com.reds.turk_system;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;


public class Bid extends Fragment { // This fragment presents the DEV users with the projects they have bidded on.
    ListView list;
    ArrayList<String> data;

    public Bid() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_bid, container, false);
        list = (ListView) root.findViewById(R.id.bid_list);
        data = getArguments().getStringArrayList("data");
        new DataRetrieval(list, 4).execute(data.get(1));
        list.setOnItemClickListener(new Project_Viewer(data));
        return root;
    }
}
