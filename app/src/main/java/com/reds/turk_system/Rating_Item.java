package com.reds.turk_system;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Ronny on 12/7/2017.
 */

public class Rating_Item extends BaseAdapter { // an item that belongs in a rating list.
private String [] data;
private Context cont;
private LayoutInflater inflater;
private TextView rate,from,desc;
private Button dispute;

    public Rating_Item(Context cont,String[] data) {
        this.data = data;
        this.cont= cont;
        inflater = (LayoutInflater) cont.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int i) {
        return data[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = view;
        if (vi == null)
            vi = inflater.inflate(R.layout.rating_row, null);
        rate = (TextView) vi.findViewById(R.id.rate);
        from = (TextView) vi.findViewById(R.id.from);
        desc =(TextView) vi.findViewById(R.id.descbox);
        dispute =(Button) vi.findViewById(R.id.disputebut);

       final String [] temp = data[i].split(",");
        rate.setText(temp[4]);
        from.setText(temp[1]);
        desc.setText(temp[3]);

        dispute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DataSender(view.getContext(),18).execute(temp[2],""+temp[0]+"'"+desc.getText(),"rate");
            }
        });



    return vi;
    }
}
