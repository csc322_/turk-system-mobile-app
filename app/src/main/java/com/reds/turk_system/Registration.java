package com.reds.turk_system;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class Registration extends AppCompatActivity { // The registration screen.
    private EditText un, em, ps, ps2, name;
    private Button reg;
    private RadioGroup choice;

    public Registration() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_registration);
        //setup
        un = (EditText) findViewById(R.id.user_in);
        em = (EditText) findViewById(R.id.email_in);
        ps = (EditText) findViewById(R.id.pass_in);
        ps2 = (EditText) findViewById(R.id.pass2_in);
        name = (EditText) findViewById(R.id.name_in);
        reg = (Button) findViewById(R.id.Register_Acc);
        choice = (RadioGroup) findViewById(R.id.usertypechoice);

        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { // The registeration button listener.
                if (choice.getCheckedRadioButtonId() == R.id.Developer || choice.getCheckedRadioButtonId() == R.id.Client) { // check the choice made by the user.
                    String type = "";
                    if (choice.getCheckedRadioButtonId() == R.id.Developer) {
                        type = "dev";
                    } else {
                        type = "client";
                    }


                    if (ps.getText().toString().compareTo(ps2.getText().toString()) == 0 && ps.getText().toString().compareTo("") != 0) { // make sure passwords match
                        new DataSender(reg.getContext()).execute(em.getText().toString(), ps.getText().toString(), name.getText().toString(), type);
                    } else {
                        ps2.setError("Passwords should match! Try Again");

                    }


                } else {

                    Toast.makeText(reg.getContext(), "Please Select a type. Developer or Client", Toast.LENGTH_SHORT).show(); // makes sure the user selects a type of user.

                }
            }

        });

    }

}
