package com.reds.turk_system;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

public class ClientSubmission extends AppCompatActivity {
    private EditText desc;
    private RatingBar rate;
    private Button submitrate;

    @Override
    protected void onCreate(Bundle savedInstanceState) { // This screen is presented to the client when he is rating a developer on his project submission
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_submission);
        desc = (EditText) findViewById(R.id.Description2);
        rate = (RatingBar) findViewById(R.id.ratingBar2);
        submitrate = (Button) findViewById(R.id.Submitrate2);
        submitrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rate.getRating() < 3 && desc.getText().toString().compareTo("") == 0) {
                    desc.setError("Explain Your Rating Please");
                } else {

                    new DataSender(submitrate.getContext(), 6).execute(getIntent().getExtras().get("clientnum").toString(), getIntent().getExtras().get("devnum"), rate.getRating() + "", desc.getText().toString());
                    if (rate.getRating() >= 3) {
                        new DataSender(submitrate.getContext(), 9).execute(getIntent().getExtras().get("projid"), getIntent().getExtras().get("clientnum"), getIntent().getExtras().get("devnum"));
                    } else {
                        new DataSender(submitrate.getContext(), 10).execute(getIntent().getExtras().get("projid"), getIntent().getExtras().get("clientnum"), getIntent().getExtras().get("devnum"), desc.getText().toString(), "pay");
                    }
                    new DataSender(submitrate.getContext(), 8).execute(getIntent().getExtras().get("projid"));
                    finish();
                }

            }
        });
    }
}
