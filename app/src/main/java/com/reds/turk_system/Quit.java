package com.reds.turk_system;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Quit extends Fragment { // The screen presented to the SU with all quit Request.
    private GridView grid;

    public Quit() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_quit, container, false);
        grid = (GridView) root.findViewById(R.id.quit_grid);
        new DataRetrieval(grid).execute("quit");
        return root;
    }

}
