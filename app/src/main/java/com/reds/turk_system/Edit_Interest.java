package com.reds.turk_system;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Edit_Interest extends AppCompatActivity { // This screen is shown to users when they are editing their interest.
    private EditText interest;
    private Button update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_interest);
        interest = (EditText) findViewById(R.id.interesttext);
        interest.setText(getIntent().getExtras().getStringArray("interest")[0]);
        update = (Button) findViewById(R.id.updateint);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DataSender(update.getContext(), 1).execute(interest.getText().toString(), getIntent().getExtras().getStringArray("interest")[1]);
            }
        });


    }
}
