package com.reds.turk_system;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class Mini_Profile_Activity extends AppCompatActivity { // the screen presented to the user when they click on another users profile.
    private TextView name, rating, bio, interest;
    private ImageView profpic;
    private ListView projs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mini__profile_);
        String[] data = getIntent().getExtras().getStringArray("userdata");
        //Setting up the Screen and the information
        name = (TextView) findViewById(R.id.Name1);
        rating = (TextView) findViewById(R.id.rating1);
        bio = (TextView) findViewById(R.id.Bio_Desc1);
        interest = (TextView) findViewById(R.id.interests1);
        profpic = (ImageView) findViewById(R.id.profilepic1);
        projs = (ListView) findViewById(R.id.proj_list1);

        name.setText(data[3]);
        rating.setText(data[7]);
        bio.setText(data[5]);
        interest.setText(data[6]);
        new DataRetrieval(projs, 3).execute(data[1]);
        Glide.with(name.getContext()).load(data[4]).into(profpic); // imports the profile picture


    }
}
