package com.reds.turk_system;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class General extends AppCompatActivity { // This screen is presented to users when they are filing a dispute.
    private EditText resp;
    private Button send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general);
        resp = (EditText) findViewById(R.id.respons);
        send = (Button) findViewById(R.id.send_resp);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DataSender(view.getContext(), 18).execute(getIntent().getExtras().get("usernum"), resp.getText() + "", "dis");
                Toast.makeText(view.getContext(), "check the warnings tab for a repsonse", Toast.LENGTH_SHORT);
                finish();
            }
        });
    }
}
